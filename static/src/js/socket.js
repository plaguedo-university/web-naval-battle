var socket = io(window.location.origin, {
    path: '/ws/socket.io/'
});

socket.on('player ready', function (result) {
    // TODO: Отобразить кнопки ПОИСК, ПРИСОЕДИНИТЬСЯ, ПРИГЛАСИТЬ
    /*
    $("#prepareButtonsBar").hide();
    $("#scale").hide();
    if (!result.gameID) {
        $("#gameTypeSelectButtonsBar").show();
    }
    */
});

socket.on('game create', function (result) {
    $("#gameTypeSelectButtonsBar").hide();
    $("#inviteLink").text(`Отправьте другу эту ссылку: ${ window.location.origin + '/ws/join/' + result.gameID}`);
    $("#inviteLink").show();
    
    //$("#prepareButtonsBar").hide();
    //$("#gameTypeSelectButtonsBar").show();
});

socket.on('player turn result', function(status) {
    if (status == "miss" || status == "fire") {
        $.growl.warning({ message: "Вы уже стреляли по этим координатам!" });
    } else if (status == "empty") {
        // TODO: Оповестить о смене хода
        cellsEnemy[enemyPosition.x][enemyPosition.y].state = -1;
    } else if (status == "hit") {
        cellsEnemy[enemyPosition.x][enemyPosition.y].state = 1;
    }
});

socket.on('enemy turn result', function (data) {
    if (data.status == "miss") {
        cells[data.coordinates.x][data.coordinates.y].state = -2;
    } else if (data.status == "hit") {
        cells[data.coordinates.x][data.coordinates.y].state = -1;
    }
});

socket.on('game finish', function (status) {
    alert('Игра закончена!');
})

socket.on('game info', function (data) {
    console.log(JSON.stringify(data, null, 4));
    document.getElementById("ship_scale").value = data.gameConfig.maxShip;
    document.getElementById("field_scale").value = data.gameConfig.mapSize.width;
    scaleHandler();
});

socket.on('player turn', function (data) {
    $.growl.warning({ message: "Ваш ход!" });
});

socket.on('game error', function (error) {
    growl_data = {
        message: error.message
    }
    $.growl.error(growl_data);
});

$(function(){
    let url = new URL(window.location.href);
    let gameID = url.searchParams.get("gameID");

    // Если мы пытаемся подключиться к игре
    if (gameID) {
        socket.emit('game info', {
            "gameID": gameID
        });
        // $("#prepareButtonsBar").hide();
        $("#scale").hide();
    }

    $("#playerReadyBtn").click(function () {
        if (areShipsReady()) {
            userData = {
                ships: Game.ships,
                mapSize: cellsCount,
                maxShip: maxShip,
                gameType: "classic"
            }

            $("#scale").hide();
            $("#prepareButtonsBar").hide();
            if (gameID) {
                userData["gameID"] = gameID;
            } else {
                $("#gameTypeSelectButtonsBar").show();
            }
            
            socket.emit('player ready', userData);
            // TODO: Отобразить процесс загрузки (?)
        } else {
            $.growl.warning({ message: "Не все корабли расставлены!" });
        }
    });

    $("#startGameBtn").click(function () {
        // TODO: Отобразить ожидание
        socket.emit('game create');
    });

   document.getElementById("enemyCanvas").addEventListener("click", function(e) {
        let pos = getPosition(e);
        enemyPosition = pos;
        socket.emit('player turn', pos);
   }, false);
});
