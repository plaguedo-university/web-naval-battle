class TGameConfig {
    constructor(map, availableShips) {
        this.map = {
            "width":  map.width,
            "height": map.height
        };
        this.ships = availableShips;
    }
}

class TGame {
    constructor(gameConfig) {
        this.gameConfig = gameConfig;
        this.turnes = [];
        this.ships = {};
    }

    canPutShip(ship) {
        let shipCoords = ship.coordinates;
        let map = this.getMap();
        // Поддерживается ли длина конфигурацией
        if (!this.gameConfig.ships.hasOwnProperty(ship.length)) {
            return false;
        }
        // Все ли корабли данной длины расставлены
        if (this.ships.hasOwnProperty(ship.length) && 
                this.ships[ship.length].length >= this.gameConfig.ships[length]) {
            return false;
        }
        // Умещается ли корабль в размер поля
        if (shipCoords.x < 0 || shipCoords.x > map.width || shipCoords.y < 0 || shipCoords.y > map.height) {
            return false;
        }
        if (ship.orient == "horizontal") {
            if ((shipCoords.x + ship.length) > map.width) {
                return 0;
            }
        } else {
            if ((shipCoords.y + ship.length) > map.height) {
                return 0;
            }
        }
        // Проверить на пересечение с другими кораблями
        for (let unitesSize in this.ships) {
            for (let s in this.ships[unitesSize]) {
                // TODO
            }
        }
        return true;
    }

    putShip(ship) {
        if(this.canPutShip(ship)) {
            if (this.ships[ship.length]) {
                this.ships[ship.length].push(ship);
            } else {
                this.ships[ship.length] = [ship];
            }
        }
    }

    getMap() {
        return this.gameConfig.map;
    }
}

class TTurn {
    constructor(type, coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    execute() {

    }
}

class TShip {
    constructor(length, coordinates, orient) {
        this.length = length;
        this.coordinates = coordinates;
        this.orient = orient;
    }
}

function ships_config(maxShip) {
    let i = maxShip;
    let result = {};
    for (i; i >= 1; i--) {
        //str = "" + i + "";
        result[i] = maxShip - i + 1;
    }
    console.log("result = ", result);
    return result;
}

var cellsCount = document.getElementById("field_scale").value;
var maxShip = document.getElementById("ship_scale").value;
var DefaultGameConfig = new TGameConfig({"width": cellsCount, "height": cellsCount}, ships_config(maxShip)/*
    "1": 4,
    "2": 3,
    "3": 2,
    "4": 1
}*/);
var Game = new TGame(DefaultGameConfig);


var canvas = document.getElementById("canv");
var canvEnemy = document.getElementById("enemyCanvas");
var ctx = canvas.getContext("2d");
var ctxEnemy = canvEnemy.getContext("2d");
var pShipsRemains = document.getElementById("ships_remains");
var pInfo = document.getElementById("error_ready_info");
var bClearField = document.getElementById("clear_field");
ctx.strokeStyle = "black";
ctxEnemy.strokeStyle = "black";


//var maxShip = 4;
var cellWidth = canvas.width / cellsCount;
var cellHeight = canvas.height / cellsCount;
var orient = 1;
var curShip = maxShip;
var currentPosition = {x: -1, y: -1};
var enemyPosition = {x: -1, y: -1};
var totalShips = [];

var cells = [];
var cellsEnemy = [];
var placeSuccess = 1;
var playerReady = 0;
var gameOn = 1;
var turn = 1;

function findNewCurShip() {
    for (let i = maxShip - 1; i >= 0; --i) {
        if (totalShips[i]) {
            curShip = i + 1;
            return;
        }
    }
    curShip = 0;
}

function clearField() {
    curShip = maxShip;
    cells = [];
    cellsEnemy = [];
    totalShips = [];
    for (let i = 0; i < cellsCount; ++i) {
        cells[i] = [];
        cellsEnemy[i] = [];
        for (let j = 0; j < cellsCount; ++j) {
            cells[i][j] = {state: 0, light: 0};
            cellsEnemy[i][j] = {state: 0, light: 0};
        }
    }
    for (let i = 0; i < maxShip; ++i) {
        totalShips[i] = maxShip - i;
    }
    findNewCurShip();
    playerReady = 0;
    placeSuccess = 1;
    pInfo.innerHTML = "";
    DefaultGameConfig = new TGameConfig({"width": cellsCount, "height": cellsCount}, ships_config(maxShip));
    Game = new TGame(DefaultGameConfig);
}

clearField();



function refreshShipsInfo() {
    let text = "Осталось кораблей: ";
    let i = 0;
    for (i; i < maxShip - 1; ++i) {
        if (i == curShip - 1) {
            text += "<b>";
        }
        text = text + (i + 1) + "-п: " + (totalShips[i]) + ", ";
        if (i == curShip - 1) {
            text += "</b>";
        }
    }
    if (i == curShip - 1) {
        text += "<b>";
    }
    text = text + maxShip + "-п: " + (totalShips[maxShip - 1]);
    if (i == curShip - 1) {
        text += "</b>";
    }
    pShipsRemains.innerHTML = text;
}


function printError() {
    if (!placeSuccess) {
        pInfo.innerHTML = "Нельзя установить такой корабль!";
        pInfo.style = "color: red";
    } else {
        pInfo.innerHTML = "";
    }
}



function drawCells() {
    for (i = 0; i < cellsCount; ++i) {
        for (j = 0; j < cellsCount; ++j) {
            let cellX = (i * (cellWidth));
            let cellY = (j * (cellHeight));
            ctx.beginPath();
            ctx.rect(cellX+1, cellY+1, cellWidth-1, cellHeight-1);
            ctx.stroke();
            if (cells[i][j].light == 1) {
                ctx.fillStyle = "yellow";
            } else if (cells[i][j].light == 2) {
                ctx.fillStyle = "red";
            } else if (cells[i][j].light == 3) {
                ctx.fillStyle = "grey";
            } else if (cells[i][j].light == 4) {
                ctx.fillStyle = "green";
            } else {
                ctx.fillStyle = "rgba(0, 0, 0, 0)";
            }
            ctx.fill();
            ctx.closePath();

            ctxEnemy.beginPath();
            ctxEnemy.rect(cellX+1, cellY+1, cellWidth-1, cellHeight-1);
            ctxEnemy.stroke();
            if (cellsEnemy[i][j].light == 1) {
                ctxEnemy.fillStyle = "yellow";
            } else if (cellsEnemy[i][j].light == 2) {
                ctxEnemy.fillStyle = "red";
            } else if (cellsEnemy[i][j].light == 3) {
                ctxEnemy.fillStyle = "grey";
            } else if (cellsEnemy[i][j].light == 4) {
                ctxEnemy.fillStyle = "green";
            } else {
                ctxEnemy.fillStyle = "rgba(0, 0, 0, 0)";
            }

            if (!playerReady) {
                ctxEnemy.fillStyle = "grey";
            }
            ctxEnemy.fill();
            ctxEnemy.closePath();
        }
    }
}


function setLight() {
    let x = currentPosition.x;
    let y = currentPosition.y;
    if (x >= 0 && y >= 0) {
        for (let i = 0; i < cellsCount; ++i) {
            for (let j = 0; j < cellsCount; ++j) {
                if (cells[i][j].state == 1) {
                    cells[i][j].light = 4;
                } else if (cells[i][j].state == -1) {
                    cells[i][j].light = 2;
                } else if (cells[i][j].state == -2) {
                    cells[i][j].light = 3;
                }
            }
        }
        let available = 1;
        if (!checkFields()) {
            available = 2;
        }
        for (let i = 0; i < curShip; ++i) {
            if (orient) {
                if (x + i < cellsCount) {
                    cells[x + i][y].light = available;
                }
            } else {
                if (y + i < cellsCount) {
                    cells[x][y + i].light = available;
                }
            }
        }
    }
    x = enemyPosition.x;
    y = enemyPosition.y;
    if (x >= 0 && y >= 0) {
        for (let i = 0; i < cellsCount; ++i) {
            for (let j = 0; j < cellsCount; ++j) {
                if (cellsEnemy[i][j].state == 1) {
                    cellsEnemy[i][j].light = 1;
                } else if (cellsEnemy[i][j].state == -1) {
                    cellsEnemy[i][j].light = 3;
                }
            }
        }
        available = 4;
        if (!turn || cellsEnemy[x][y].state != 0) {
            available = 2;
        }
        cellsEnemy[x][y].light = available;
        //console.log("painted");
    }
}

function max(a, b) {
    if (a > b) {
        return a
    } else {
        return b;
    }
}

function min(a, b) {
    if (a < b) {
        return a
    } else {
        return b;
    }
}

function clearLights() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctxEnemy.clearRect(0, 0, canvas.width, canvas.height);
    for (i = 0; i < cellsCount; ++i) {
        for (j = 0; j < cellsCount; ++j) {
            cells[i][j].light = 0;
            cellsEnemy[i][j].light = 0;
        }
    }
}

function checkFields() {
    let x = currentPosition.x, y = currentPosition.y;
    if (x < 0 || y < 0 || x >= cellsCount || y >= cellsCount) {
        return 0;
    }
    if (orient) {
        if (x + curShip > cellsCount) {
            return 0;
        }
    } else {
        if (y + curShip > cellsCount) {
            return 0;
        }
    }

    if (orient) {
        for (let i = max(0, x - 1); i < min(x + curShip + 1, cellsCount); i++) {
            for (let j = max(y - 1, 0); j <= min(y + 1, cellsCount - 1); j++){
                //console.log("i = " + i + ", j = " + j + ", cc = " + cellsCount);
                if (cells[i][j].state) {
                    return 0;
                }
            }
        }
    } else {
        for (let i = max(0, y - 1); i < min(y + curShip + 1, cellsCount); i++) {
            for (let j = max(0, x - 1); j <= min(x + 1, cellsCount - 1); j++) {
                //console.log("i = " + i + ", j = " + j + ", cc = " + cellsCount);
                if (cells[j][i].state) {
                    return 0;
                }
            }
        }
    }

    return 1;

}


function areShipsReady() {
    res = 1;
    for (let i = 0; i < maxShip; ++i) {
        if (totalShips[i]) {
            res = 0;
        }
    }
    return res;
}

function draw() {
    clearLights();
    setLight();
    drawCells();
    refreshShipsInfo();
    printError();
    if (areShipsReady()) {
        placeSuccess = 1;
        playerReady = 1;
        pInfo.innerHTML = "Все корабли расставлены!";
        pInfo.style = "color: black";
    }
}


function getPosition(e) {
    let x = e.x;
    let y = e.y;
    let cnv = e.target;
    let pos = {x: 0, y: 0};

    x -= cnv.offsetLeft;
    y -= cnv.offsetTop;

    pos.x = Math.floor(x / cellWidth);
    pos.y = Math.floor(y / cellHeight);
    return pos;
    //cellX = Math.floor(x / cellWidth);
    //cellY = Math.floor(y / cellHeight);
    //currentPosition.x = cellX; 
    //currentPosition.y = cellY; 

}

function playerFieldMove(e) {
    let pos = getPosition(e);

    //console.log("PL = " + pos.x + " " + pos.y);
    currentPosition = pos;
}

function enemyFieldMove(e) {
    let pos = getPosition(e);
    //console.log("EN = " + pos.x  + " " + pos.y);
    enemyPosition = pos;
}

function playerFieldClick(e) {
    let pos = getPosition(e);
    currentPosition = pos;
    placeShip(e);
}



function keyUpHandler(e) {
    for (let i = 0; i < 8; ++i) {
        if (e.key == (i + 1) && totalShips[i] && maxShip >= (i + 1)) {
            curShip = i + 1;
        }
    }
    if (e.key == "q") {
        if (orient) orient = 0
        else orient = 1;
    }
    refreshShipsInfo();
}


function placeShip(e) {
    if (checkFields(1) && totalShips[curShip - 1]) {
        for (let i = 0; i < curShip; ++i) {
            if (orient) {
                if (currentPosition.x + i < cellsCount) {
                    cells[currentPosition.x + i][currentPosition.y].state = 1;
                }
            } else {
                if (currentPosition.y + i < cellsCount) {
                    cells[currentPosition.x][currentPosition.y + i].state = 1;
                }
            }
        }
        Game.putShip(new TShip(curShip, {
            "x": currentPosition.x,
            "y": currentPosition.y
        }, (orient == 1 ? "horizontal" : "vertical")));

        totalShips[curShip - 1] -= 1;
        if (!totalShips[curShip - 1]) {
            findNewCurShip();
        }
        placeSuccess = 1;
        refreshShipsInfo();
    } else {
        if (!playerReady) {
            placeSuccess = 0;
        }
    }

}

function scaleHandler() {
    cellsCount = document.getElementById("field_scale").value;
    maxShip = document.getElementById("ship_scale").value;
    cellWidth = canvas.width / cellsCount;
    cellHeight = canvas.height / cellsCount;
    currentPosition = {x: -1, y: -1};
    enemyPosition = {x: -1, y: -1};
    clearField();
    refreshShipsInfo();
    findNewCurShip();
    setLight();
}

canvas.addEventListener("mousemove", playerFieldMove, false);
canvas.addEventListener("click", playerFieldClick, false);
canvEnemy.addEventListener("mousemove", enemyFieldMove, false);
document.addEventListener("keyup", keyUpHandler, false);
document.getElementById("field_scale").addEventListener("input", scaleHandler, false);
document.getElementById("ship_scale").addEventListener("input", scaleHandler, false);
bClearField.addEventListener("click", clearField, false);

var interval = setInterval(draw, 10);
