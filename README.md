# Docker (Да, это круто!)

- `sudo docker-compose up -d --build` - Поднять сервисы (контейнеры).
- `sudo docker-compose ps` - Посмотреть статус контейнеров.
- `sudo docker exec -it <container_hash> sh`- Подключиться к шеллу контейнера. Узнать хеш можно с помощью `sudo docker ps`.
- `sudo docker container logs <name>` - Посмотреть логи.

# Об игре

Игра не создастся и нельзя подключится к игре, если игрок не расставил корабли.



Первое событие, отправленное клиентом - `player ready`, которое передаёт конфигурацию кораблей на сервер. Если всё валидно, то сервер присваивает полю `isReady` значение `true`. 

События:

- game create
- game join
- game search
- game starting
- game ending
- player ready
- player turn
- player hit

```javascript
socket.Player = {
    isReady: true | false,            // Готов ли игрок
    gameID: 'GAME_ID',                // ID игры, к которой присоединён игрок
    configType: 'classic' | 'custom', // Тип игры
    battleField: Array[Array[]]       // Поле игрока
}
```

