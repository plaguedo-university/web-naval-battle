const shortUUID = require('short-uuid');
const TGame = require('./TGame');

class TGameEngine {
    constructor() {
        this.games = {};
        this.queue = [];
    }

    createGame(socket, config) {
        let newGameId = shortUUID.generate();
        let newGame = new TGame(newGameId, config);
        console.log(`Создана игра с ID: ${newGameId}`);
        newGame.addPlayer(socket);
        this.games[newGameId] = newGame;
        return newGameId;
    }

    joinGame(socket, id) { 
        if (this.games.hasOwnProperty(id)) {
            let game = this.games[id];
            console.log(`К игре ${id} подключается игрок...`);
            if (game.canPlayerJoin()) {
                game.addPlayer(socket);
                game.start(); // TODO
            }
        } else {
            // TODO: error
        }
    }

    findGame(socket) {

    }

    getGameByID(id) {
        if (this.games.hasOwnProperty(id)) {
            return this.games[id];
        } else {
            return null;
        }
    }
}

module.exports = TGameEngine;