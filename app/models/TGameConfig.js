class TGameConfig {
    constructor(map, availableShips) {
        this.map = {
            "width": map.width,
            "height": map.height
        };
        this.ships = availableShips;
    }
}

module.exports = TGameConfig;