class TBattlefield {
    constructor(mapSize, maxShip) { // TODO: Добавить конфиг тут (?)
        // Сформировать поле
        this.cells = new Array(mapSize.height);
        for (let i = 0; i < this.cells.length; i++) {
            this.cells[i] = new Array(mapSize.width);
        }
        for (var i = 0; i < this.cells.length; i++) {
            for (var j = 0; j < mapSize.width; j++) {
                this.cells[i][j] = {
                    status: "empty",
                    ship: null
                };
            }
        }

        this.mapSize = mapSize;
        this.ships = [];
        this.maxShip = maxShip;
    }

    putShip(ship) {
        // TODO: Проверить, можно ли поставить корабль
        // canPutShip()
        
        if (ship.orient == "horizontal") {
            for (let i = 0; i < ship.length; i++) {
                let cell = this.cells[ship.coordinates.y][ship.coordinates.x + i];
                cell.status = "ship";
                cell.ship = ship;
            }
        } else {
            for (let i = 0; i < ship.length; i++) {
                let cell = this.cells[ship.coordinates.y + i][ship.coordinates.x];
                cell.status = "ship";
                cell.ship = ship;
            }
        }
        this.ships.push(ship);
    }

    checkEndGame() {
        let gameOver = true;
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                if (this.cells[i][j].status == "ship") {
                    gameOver = false;
                    break;
                }
            }
        }
        return gameOver;
    }

    getCell(coordinates) {
        return this.cells[coordinates.y][coordinates.x];
    }
}

module.exports = TBattlefield;
