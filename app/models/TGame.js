// TODO: Вынести в модуль utils
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

class TGame {
    constructor(gameId, gameConfig) {
        console.log(gameConfig);
        
        this.gameId = gameId;
        this.gameConfig = gameConfig;
        this.gameStatus = "free";
        this.turnes = [];
        this.players = [];
        this.nowTurnForPlayer = null;
    }

    start() {
        this.gameStatus = "active";
        this.nowTurnForPlayer = this.players[getRandomInt(2)]; // Выбираем, кто ходит первым
        this.emitToAllPlayers('game starting');
        this.nowTurnForPlayer.emit('player turn');
    }

    addPlayer(socket) {
        // TODO: Добавить механику наблюдателя. 
        // Типа если игроков больше, чем нужно - тот, 
        // кто попытается подключиться - сделать observer
        this.players.push(socket);
        if (this.players.length === 2) {
            this.players[0].Player.enemy = this.players[1];
            this.players[1].Player.enemy = this.players[0];
        }
    }

    makeTurn(socket, turn) {
        // TODO: Проверить координаты, попадают ли они в размер поля
        if (socket != this.nowTurnForPlayer) {
            socket.emit('game error', {
                message: "Сейчас не ваш ход!"
            })
            return;
        }
        // TODO: Добавить автора к turn
        let enemyBattlefield = socket.Player.enemy.Player.battlefield;
        let cell = enemyBattlefield.getCell(turn.coordinates);
        let cellStatus = cell.status;
        switch(cellStatus) {
            case 'miss':
                socket.emit('player turn result', cellStatus);
                break;
            case 'fire':
                socket.emit('player turn result', cellStatus);
                break;
            case 'empty':
                // Установить промах
                cell.status = "miss";
                // Оповестить о промахе
                socket.emit('player turn result', cellStatus);
                socket.Player.enemy.emit('enemy turn result', {
                    "status": "miss",
                    "coordinates": turn.coordinates
                });
                // Передать ход
                this.nowTurnForPlayer = this.nowTurnForPlayer.Player.enemy;
                this.nowTurnForPlayer.emit('player turn');
                break;
            case 'ship':
                cell.status = "fire";
                cell.ship.healthPoint -= 1;
                // TODO: Проверить, убит ли корабль
                // TODO: Оповестить о попадании
                this.nowTurnForPlayer.emit('player turn result', "hit");
                socket.Player.enemy.emit('enemy turn result', {
                    "status": "hit",
                    "coordinates": turn.coordinates
                });
                if (enemyBattlefield.checkEndGame()) {
                    this.emitToAllPlayers('game finish');
                }
                break;
        }
    }


    isPlayerInGame(socket) {
        for (let i = 0; i < this.players.length; i++) {
            if (socket == this.players[i]) {
                return true;
            }
        }
        return false;
    }

    canPlayerJoin() {
        // TODO: Проверить состояние игры и игроков
        return true;
    }

    emitToAllPlayers(event, data) {
        for (let i = 0; i < this.players.length; i++) {
            this.players[i].emit(event, data);
        }
    }
}

module.exports = TGame;
