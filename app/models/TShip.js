class TShip {
    constructor(length, coordinates, orient) {
        this.length = length;
        this.healthPoints = length;
        this.coordinates = coordinates;
        this.orient = orient;
    }
}

module.exports = TShip;