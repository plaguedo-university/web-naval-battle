const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);

// Import Models
let TGameConfig = require('./models/TGameConfig');
let TGameEngine = require('./models/TGameEngine');
let TBattlefield = require('./models/TBattlefield');
let TTurn = require('./models/TTurn');
let TShip = require('./models/TShip');

// Default settings
let GameEngine = new TGameEngine();
let defaultGameConfig = new TGameConfig({
    "width": 10,
    "height": 10
}, {
    "1": 4,
    "2": 3,
    "3": 2,
    "4": 1
});

server.listen(process.env.PORT || 8800); //5553535

app.get("/join/:gameID", (req, res) => {
    // TODO: найти игру и проверить валидность
    res.redirect(`/?gameID=${req.params.gameID}`);
});

io.on('connection', function (socket) {
    // TODO: Добавить middlewhere на проверку количества активных соединений для данного IP
    socket.Player = {
        isReady: false
    };

    socket.on('game create', function (data) {
        // Check games by IP, Token generate and send back
        // TODO: Также проверить, нет ли у игрока уже активной игры
        if (socket.Player.isReady) {
            let gameID = GameEngine.createGame(socket, {
                maxShip: socket.Player.battlefield.maxShip,
                mapSize: socket.Player.battlefield.mapSize
            });
            socket.Player.activeGameID = gameID;
            socket.emit('game create', {
                "gameID": gameID
            });
        } else {
            socket.emit('game error', {
                message: 'Нельзя создать игру. Статус игрока - не готов.'
            });
        }
    });

    socket.on('game join', function (data) {
        // Token in data. Check game by token and connect
        // Check player ready status, don't start without readystatus
        console.log(data);
    });

    socket.on('game search', function (data) {
        // Token in data. Check game by token and connect
        // Check player ready status, don't start without readystatus
        console.log(data);
    });

    socket.on('game info', function (data) {
        // TODO: Показывать только не активные, ожидающие игры, 
        // или когда ты сам в этой игре
        let game = GameEngine.getGameByID(data.gameID);
        if (game === null) {
            socket.emit('game error', {
                message: 'Игры, к которой вы пытаетесь подключиться - не существует.'
            });
            return;
        }
        console.log({
            gameConfig: game.gameConfig,
            gameStatus: game.gameStatus
        });
        
        socket.emit('game info', {
            gameConfig: game.gameConfig,
            gameStatus: game.gameStatus
        });
    });

    socket.on('player ready', function (data) {
        // TODO: Проверить валидность кораблей
        // TODO: Ввести модель игрока
        // TODO: Проверить в игре ли уже игрок
        let newBF = new TBattlefield({
            width: parseInt(data.mapSize),
            height: parseInt(data.mapSize)
        }, parseInt(data.maxShip)); // TODO: Тут конфигурировать походу
        for (let i in data.ships) {
            for (let j in data.ships[i]) {
                let userShip = data.ships[i][j];
                let ship = new TShip(userShip.length, userShip.coordinates, userShip.orient);
                newBF.putShip(ship);
            }
        }
        // Если всё ок:
        socket.Player.isReady = true;
        socket.Player.configType = data.gameType;
        if (data.gameType === "classic") {
            socket.Player.battlefield = newBF;
        }
        // console.log(JSON.stringify(socket.Player.battlefield, null, 4));
        
        let gameID = data.gameID;
        if (gameID) {
            socket.Player.activeGameID = gameID;
            GameEngine.joinGame(socket, gameID);
        }
        socket.emit('player ready', {
            status: "ok"
        });
    });

    socket.on('player turn', function (data) {
        if (!socket.Player.isReady) {
            socket.emit('game error', {
                message: 'Нельзя сделать ход. Статус игрока - не готов.'
            });
            return;
        }
        let game = GameEngine.getGameByID(socket.Player.activeGameID);
        if (game === null) {
            socket.emit('game error', {
                message: 'Игры, в которой вы пытаетесь сделать ход - не существует.'
            });
            return;
        }
        if (!game.isPlayerInGame(socket)) {
            socket.emit('game error', {
                message: 'Вы не подключены к этой игровой сессии.'
            });
            return;
        }
        if (game.gameStatus !== "active") {
            socket.emit('game error', {
                message: 'Игра, в которой вы пытаетесь сделать ход - неактивна.'
            });
            return;
        }
        if (!Number.isInteger(data.x) || !Number.isInteger(data.y)) {
            socket.emit('game error', {
                message: 'Невалидные координаты хода.'
            });
            return;
        }
        let newTurn = new TTurn({
            x: data.x,
            y: data.y
        });
        game.makeTurn(socket, newTurn);
    });

    socket.on('player hit', function (data) {
        // 
        console.log(data);
    });



    socket.on('disconnect', function(){
        socket.Player.isReady = false;
        console.log('user disconnected');
    });

    socket.emit('news', { hello: 'world' });
});